import sys
import re
import numpy as np
import copy
import random
import elog
import io
import os
import datetime

from PyQt5 import QtWidgets,QtGui
from PyQt5.uic import loadUiType
from PyQt5 import QtCore


from matplotlib.figure import Figure
import matplotlib.pyplot as plt

from matplotlib.backends.backend_qt5agg import (
    FigureCanvasQTAgg as FigureCanvas,
    NavigationToolbar2QT as NavigationToolbar)


sys.path.append('/sf/bd/applications/OnlineModel/current')
import OMFacility
import AcquisitionCore

import ChannelManager

Ui_BDCorrGUI, QMainWindow = loadUiType('BDCorr.ui')




class BDCorrelator(QMainWindow, Ui_BDCorrGUI):
    def __init__(self):
        super(BDCorrelator, self).__init__()
        self.setupUi(self)
        self.setWindowIcon(QtGui.QIcon("rsc/icontraj.png"))
        self.setWindowTitle("BSRead Channel Analysis and Correlation")

        self.initmpl()
        self.version='1.0.1'

        self.SF=OMFacility.Facility(1,0)
    
        self.bschannels=[]
        self.bpm=[]
        for ele in self.SF.ElementDB.keys():
            if 'DBPM' in ele:
                if not 'SAT' in ele:
                    self.bpm.append(ele.replace('.','-'))


        self.CM=ChannelManager.ChannelManager()
        self.updateTree()

        self.ac=AcquisitionCore.Acquisition()
        
        self.UIStart.clicked.connect(self.start)
        self.UIStop.clicked.connect(self.stop)
        self.UIPause.clicked.connect(self.pause)
        self.UIFlush.clicked.connect(self.flush)
        self.save.clicked.connect(self.logbook)

        self.UIChannel.currentIndexChanged.connect(self.updateTree)
        self.UISave.clicked.connect(self.saveConfig)
        self.UILoad.clicked.connect(self.loadConfig)
        self.UIAnalysis.currentIndexChanged.connect(self.updateConfig)
        self.UITree.itemClicked.connect(self.updateConfig)
        self.UIAddRef.clicked.connect(self.addRefConfig)
        self.UIRemoveRef.clicked.connect(self.removeRefConfig)

        self.timer=QtCore.QTimer()
        self.timer.timeout.connect(self.timerEvent)

        self.plot1={}
        self.plot2={}
        self.plot3={}
        self.plot4={}

        quit = QtWidgets.QAction("Quit", self)
        quit.triggered.connect(self.closeEvent)


    def closeEvent(self,event):
        self.stop()
        print('Closing window')


    def pause(self):
        self.ac.togglePause()

    def flush(self):
        self.ac.flush()

    def iterateBSChannels(self,data,plotdata,tag):
        for key in data.keys():
            if isinstance(data[key],dict):
                tage=tag+key+"-"
                self.iterateBSChannels(data[key],plotdata,tage)
            else:
                val=data[key]
                if val:
                    if key not in self.bschannels:
                        self.bschannels.append(key)
                    if tag[0:-1] in plotdata.keys():
                        plotdata[tag[0:-1]].append(self.bschannels.index(key))
                    else:
                        plotdata[tag[0:-1]]=[self.bschannels.index(key)]

    def getBSChannels(self):
        self.bschannels.clear()
        self.plot1.clear() 
        self.iterateBSChannels(self.CM.config[0],self.plot1,"")
        self.plot2.clear() 
        self.iterateBSChannels(self.CM.config[1],self.plot2,"")
        self.plot3.clear() 
        self.iterateBSChannels(self.CM.config[2],self.plot3,"")
        self.plot4.clear() 
        self.iterateBSChannels(self.CM.config[3],self.plot4,"")
 
    def start(self):
        nsam=int(str(self.UISample.text()))
        self.plot1.clear()
        self.plot2.clear()
        self.plot3.clear()
        self.plot4.clear()
        self.getBSChannels()
        self.ac.setup(self.bschannels,nsam)
        self.ac.start()
        self.timer.start(2000)

    def stop(self):
        self.ac.stop()
        self.timer.stop()
        self.UIStatus.setText('Stopped')

  
    def timerEvent(self):

        if not self.ac.isRunning: 
            self.canvas.draw()
            return

        imax=self.ac.count
        if self.ac.count > self.ac.nsam:
            imax=self.ac.nsam
        if imax < 10:
            self.canvas.draw()
            return

        self.Log.clear()
        self.Log.appendPlainText('Statistics for %d shots' % (imax))
        self.switchPlots(0, self.axes1,self.plot1,imax)
        self.switchPlots(1, self.axes2,self.plot2,imax)
        self.switchPlots(2, self.axes3,self.plot3,imax)
        self.switchPlots(3, self.axes4,self.plot4,imax)

        self.canvas.draw()


    def switchPlots(self,iplot,axe,pidx,imax):

        style=self.CM.analysis[iplot]
        self.Log.appendPlainText('Plot %d:' % (iplot+1))
        if style==0:
            return
        elif style==1:
            self.plotHist(axe,pidx,imax,False)
        elif style==2:
            self.plotHist(axe,pidx,imax,True)
        elif style==3:
            self.plotHistStacked(axe,pidx,imax)
        elif style==4:
            self.plotCorr(axe,pidx,imax)
        elif style==5:
            self.plotFreq(axe,pidx,imax)
        elif style==6:
            self.plotTime(axe,pidx,imax,False)
        elif style==7:
            self.plotTime(axe,pidx,imax,True)
        elif style==8:
            self.plotScatter(axe,pidx,imax,True)


    def plotHistStacked(self,axe,pidx,imax):
        if len(pidx) <= 0:
            return
        axe.clear()
        for key in pidx.keys():
            sig=[]
            avg=[]
            for i in pidx[key]:
                dat=self.ac.data[0:imax,i]
                sig.append(np.std(dat))
                avg.append(np.mean(dat))
            ii=np.argmax(sig)
            self.Log.appendPlainText('Maximum RMS for %s' % key)
            self.Log.appendPlainText('%s: %7.4f (rms)' % (self.ac.channels[pidx[key][ii]],sig[ii]))
            axe.plot(sig,drawstyle='steps-mid',label=key)
        axe.set_title('RMS Fluctuation')
        axe.set_xlabel('Channels')
        axe.set_ylabel('RMS Value')
        axe.legend()



    def plotHist(self,axe,pidx,imax,center=True):
        if len(pidx) <= 0:
            return
            
        sig=[]
        avg=[]
        scl=1
        if center:
            scl=0
        for key in pidx.keys():
            for i in pidx[key]:
                dat=self.ac.data[0:imax,i]
                sig.append(np.std(dat))
                avg.append(np.mean(dat))
        range=[np.min(avg)*scl-3*np.max(sig),np.max(avg)*scl+3*np.max(sig)]
#        range = 
        axe.clear()
        for key in pidx.keys():
            for icount,i in enumerate(pidx[key]):
                axe.hist(self.ac.data[0:imax,i]-(1-scl)*avg[icount],histtype='step',bins=50,label=self.ac.channels[i])
                self.Log.appendPlainText('%s : %7.4f (rms)' % (self.ac.channels[i],sig[icount]) )

        axe.set_title('Histogram')
        axe.set_xlabel('Values')
        axe.set_ylabel('#Counts')
 #       axe.legend()


    def plotFreq(self,axe,pidx,imax):
        if len(pidx) <=0:
            return

        axe.clear()
        id=self.ac.data[0:imax,-1]
        id=id-np.min(id)
        fmax=50./(id[1]-id[0])
        
        for key in pidx.keys():
            for i in pidx[key]:
                dat=self.ac.data[0:imax,i]
                spec=np.abs(np.fft.rfft(dat))
                spec[0]=0
                ndat=len(spec)
                freq=np.linspace(0,fmax,num=ndat)
                axe.plot(freq,spec/ndat,label=self.ac.channels[i])
                ii=np.argmax(spec)
                self.Log.appendPlainText('Maximum Frequency for %s:' % self.ac.channels[i])
                self.Log.appendPlainText('  %7.3f (Hz)' % freq[ii] )
                
        axe.set_title('Spectrum')
        axe.set_xlabel('f (Hz)')
        axe.set_ylabel('Signal(f)')
        axe.legend()

    def plotTime(self,axe,pidx,imax,center=True):
        if len(pidx) <=0:
            return

        axe.clear()
        id=self.ac.data[0:imax,-1]
        id=id-np.min(id)
        
        for key in pidx.keys():
            for i in pidx[key]:
                dat=self.ac.data[0:imax,i]
                if center:
                    dat=dat-np.mean(dat)
                axe.plot(id,dat,label=self.ac.channels[i])
                sig=np.std(dat)
                self.Log.appendPlainText('%s : %7.4f (rms)' % (self.ac.channels[i],sig) )
        axe.set_title('Time Profile')
        axe.set_xlabel('Shots')
        axe.set_ylabel('Signal')
        axe.legend()


    def plotScatter(self,axe,pidx,imax,Center=True):
        if len(pidx) <=0:
            return
        axe.clear()        
        ref=pidx['Reference']
        if len(ref) <=0:
            return

        for iref in ref:
            datref=self.ac.data[0:imax,iref]
            for key in pidx.keys():
                if key == 'Reference':
                    continue
                for i in pidx[key]:
                    dat=self.ac.data[0:imax,i]
                    label='%s / %s - Odd' % (self.ac.channels[iref],self.ac.channels[i])
                    axe.scatter(datref[0::2],dat[0::2],s=0.5,label=label)
                    label='%s / %s - Even' % (self.ac.channels[iref],self.ac.channels[i])

                    axe.scatter(datref[1::2],dat[1::2],s=0.5,label=label)
        axe.set_title('Scatter Plot')
        axe.set_xlabel('Reference')
        axe.set_ylabel('Channels')
        axe.legend()


    def plotCorr(self,axe,pidx,imax):

        if len(pidx) <=0:
            return
        axe.clear()
        
        ref=pidx['Reference']
        if len(ref) <=0:
            return

        axe.clear()
        for iref in ref:
            datref=self.ac.data[0:imax,iref]
            for key in pidx.keys():
                r=[]
                if key == 'Reference':
                    continue
                for i in pidx[key]:
                    dat=self.ac.data[0:imax,i]
                    Rr=np.abs(np.corrcoef(datref,dat)[0,1])
                    r.append(Rr)
                label='%s / %s' % (self.ac.channels[iref],key)
                axe.plot(r,label=label)
        axe.set_title('Correlation Coefficient')
        axe.set_ylabel('r')
        axe.set_xlabel('Channels')
        axe.legend()


  

    def logbook(self):
        self.LP=io.BytesIO()
        self.fig.savefig(self.LP,format='png')
        self.LP.seek(0)

        year=datetime.datetime.now().strftime('%Y')
        month=datetime.datetime.now().strftime('%m')
        day=datetime.datetime.now().strftime('%d')

        path='/sf/data/measurements/%s' % year
        if not os.path.exists(path):
            os.makedirs(path)
        path='%s/%s' % (path,month)
        if not os.path.exists(path):
            os.makedirs(path)
        path='%s/%s' % (path,day)
        if not os.path.exists(path):
            os.makedirs(path)
        datetag=datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S_%f')
        filename=('%s/BSCorrelator_%s.png' % (path,datetag))
        attch=[filename]
        with open(filename,'wb') as f:
            f.write(self.LP.read())


        print('Data set saved')
        logbook = elog.open('https://elog-gfa.psi.ch/SwissFEL+commissioning+data/',user='robot',password='robot')
        text = str(self.Log.toPlainText())
        dict_att = {'Author': 'sfop', 'Application':'BSCorrelator','Category' : 'Measurement', 'Title':'Correlation Measurement'}
        new_msg_id = logbook.post(text, attributes = dict_att,attachments = attch)
        print('\nFigure was saved in ELOG and data saved in h5 file')



#------------------------
# initializing matplotlib
    def initmpl(self):

        self.fig=Figure()
        self.axes1=self.fig.add_subplot(221)
        self.axes2=self.fig.add_subplot(222)
        self.axes3=self.fig.add_subplot(223)
        self.axes4=self.fig.add_subplot(224)
        self.canvas = FigureCanvas(self.fig)
        self.mplvl.addWidget(self.canvas)
        self.canvas.draw()
        self.toolbar=NavigationToolbar(self.canvas,self.mplwindow, coordinates=True)
        self.mplvl.addWidget(self.toolbar)
 

    def iterateTree(self,root):
        croot=root.childCount()
        if croot > 0:
            dat={}
            for i in range(croot):
                child=root.child(i)
                cname=str(child.text(0))
                retdat=self.iterateTree(child)
                dat[cname]=retdat
            return dat
        else:
            cs=root.checkState(0)
            flag=False
            if cs >0:
                flag=True
            return flag


    def addRefConfig(self):
        tag=str(self.UITree.currentItem().text(0))
        if ':' in tag:
            idx=self.UIChannel.currentIndex()
            self.CM.config[idx]['Reference'][tag]=True
            self.updateTree()



    def removeRefConfig(self):
        idx=self.UIChannel.currentIndex()
        self.CM.config[idx]['Reference']={'none':False}
        self.updateTree()

        
    def saveConfig(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        fileName, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Save Configuration",
                                                            self.CM.path + '/newConfig.json',
                                                            "Json Files (*.json)", options=options)
        if not fileName:
            return
        self.CM.saveConfig(fileName)


    def loadConfig(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Open Configuration",
                                                            self.CM.path + '/newConfig.json',
                                                            "Json Files (*.json)", options=options)
        if not fileName:
            return

        self.CM.loadConfig(fileName)
        print(self.CM.config[1]['Reference'])
        self.updateTree()
            
    def updateConfig(self):
        idx=self.UIChannel.currentIndex()
        iana=self.UIAnalysis.currentIndex()
        self.CM.analysis[idx]=iana
        root = self.UITree.invisibleRootItem()
        self.CM.config[idx]=self.iterateTree(root)

    def populateTree(self,root,data):
        for key in data.keys():
            child= QtWidgets.QTreeWidgetItem(root, [key])   
            child.setFlags(child.flags() | QtCore.Qt.ItemIsTristate | QtCore.Qt.ItemIsUserCheckable )
            if isinstance(data[key],dict):
                self.populateTree(child,data[key])
            else:
                val=data[key]
                if val:
                    child.setCheckState(0,QtCore.Qt.Checked)
                else:
                    child.setCheckState(0,QtCore.Qt.Unchecked)

    def updateTree(self):
        idx=self.UIChannel.currentIndex()
        iana=self.CM.analysis[idx]
        self.UIAnalysis.blockSignals(True)
        self.UIAnalysis.setCurrentIndex(iana)
        self.UIAnalysis.blockSignals(False)
        self.UITree.blockSignals(True)
        self.UITree.clear()
        header = QtWidgets.QTreeWidgetItem(["BS Channels"])
        self.UITree.setHeaderItem(header)   #Another alternative is setHeaderLabels(["Tree","First",...])
        config=self.CM.config[idx]
        root = self.UITree.invisibleRootItem()
        root.setFlags(root.flags() | QtCore.Qt.ItemIsTristate | QtCore.Qt.ItemIsUserCheckable )
        self.populateTree(root,config)
        self.UITree.blockSignals(False)


 
    # --------------------------------
    # Main routine


if __name__ == '__main__':
    QtWidgets.QApplication.setStyle(QtWidgets.QStyleFactory.create("plastique"))
    app = QtWidgets.QApplication(sys.argv)
    main = BDCorrelator()
    main.show()
    sys.exit(app.exec_())
