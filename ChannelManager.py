import sys
sys.path.append('/sf/bd/applications/OnlineModel/current')
import OMFacility
import json

class ChannelManager:
    def __init__(self):


        self.path='/sf/data/applications/BD-BSCorrelator'
        self.bpm=[]
        self.SF=OMFacility.Facility(1,0)
        self.pollLayout()

        self.config=[]
        self.analysis=[]

        # code to generate the default file
        self.analysis=[0,0,0,0]   # rms, none, none, none
        self.config=[self.initConfig(),self.initConfig(),self.initConfig(),self.initConfig()]


#        self.loadConfigOld()
#        self.saveConfig() 

        self.loadConfig()
        self.analysis=[5,3,4,0]   # rms, none, none, none
        self.bschn=[]


    def saveConfig(self,name=None):
        if name==None:
            name='%s/default.json' % self.path

        dumpdata={}
        for i in range(4):
            maintag='Plot%d' % (i+1)
            dumpdata[maintag]={'Analysis':self.analysis[i],'Configuration':self.config[i]}
        with open(name,'w') as out:
            json.dump(dumpdata,out,indent=4)
        


    def loadConfig(self,name=None):
        if name==None:
            name='%s/default.json' % self.path
        with open(name,'r') as input:
            data=json.load(input)
        for i in range(4):
            maintag='Plot%d' % (i+1)
            self.analysis[i]=data[maintag]['Analysis']
            conf=self.initConfig()
            lconf=data[maintag]['Configuration']
            self.config[i]=self.iterateConfig(conf,lconf)
            
            
    def iterateConfig(self,conf,lconf):
        for key in lconf.keys():
            if isinstance(lconf[key],dict):
                if key in conf.keys():
                    conf[key]=self.iterateConfig(conf[key],lconf[key])
            else:
 #               if key == 'Reference':
 #                   print('Reference:',conf.keys())
 #               if key in conf.keys():
                    
                conf[key]=lconf[key]
        return conf




    def pollLayout(self):
        self.bpm.clear()
        for ele in self.SF.ElementDB.keys():
            if 'DBPM' in ele:
                self.bpm.append(ele.replace('.','-'))


    def initConfig(self):
        channels={}
        chan0=['SIN-CVME-TIFGUN-EVR0:BUNCH-1-OK','SIN-CVME-TIFGUN-EVR0:BUNCH-2-OK']  # fundamental channels at 100 Hz

        channels['System']={}
        channels['System']['SIN-CVME-TIFGUN-EVR0:BUNCH-1-OK']=True
        channels['System']['SIN-CVME-TIFGUN-EVR0:BUNCH-2-OK']=False


        channels['Photonics']={}
        channels['Photonics']['SATFE10-PEPG046-EVR0:CALCI']=False
        channels['Photonics']['SARFE10-PBIG050-EVR0:CALCI']=False

        channels['RF']={}
        channels['RF']['Injector']={}
        channels['RF']['Injector']['SINEG01-RLLE-DSP:AMPLT-VS']=False
        channels['RF']['Injector']['SINEG01-RLLE-DSP:PHASE-VS']=False
        channels['RF']['Athos']={}
        channels['RF']['Athos']['SATMA02-RLLE-DSP:PHASE-VS']=False       
        channels['BPM']={}
        locs=['Injector','Linac1','Linac2','Linac3','Aramis','Athos']
        for loc in locs:
            channels['BPM'][loc]={}
            buns=['Bunch1','Bunch2']
            for bun in buns:
                channels['BPM'][loc][bun]={}
                channels['BPM'][loc][bun]['X']={}
                channels['BPM'][loc][bun]['Y']={}
                channels['BPM'][loc][bun]['Q']={}

        for ele in self.bpm:
            loc='Injector'
            if 'S10' in ele:
                loc='Linac1'
            elif 'S20' in ele:
                loc='Linac2'
            elif 'S30' in ele:
                loc='Linac3'
            elif 'SAR' in ele:
                loc='Aramis'
            elif 'SAT' in ele:
                loc='Athos'
            channels['BPM'][loc]['Bunch1']['X']['%s:X1' % ele]=False
            channels['BPM'][loc]['Bunch1']['Y']['%s:Y1' % ele]=False
            channels['BPM'][loc]['Bunch1']['Q']['%s:Q1' % ele]=False
            channels['BPM'][loc]['Bunch2']['X']['%s:X2' % ele]=False
            channels['BPM'][loc]['Bunch2']['Y']['%s:Y2' % ele]=False
            channels['BPM'][loc]['Bunch2']['Q']['%s:Q2' % ele]=False


        channels['Reference']={'none':False}


        return channels




############################3
# old routines

    def getChannels(self,iplot):
        self.bschn.clear()
        self.recChannels(self.config[iplot])
        return self.bschn

    def recChannels(self,cnfg):
        if isinstance(cnfg,bool):
            return cnfg
        else:
            for key in cnfg.keys():
                add=self.recChannels(cnfg[key])
                if add:
                    self.bschn.append(key)
            return False


 

