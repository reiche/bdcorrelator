import numpy as np
import logging
import socket
import datetime
import random
from threading import Thread

#Qt specific libraries
from PyQt5.QtCore import pyqtSignal,QThread

#PSI libraries for bsread
from bsread import source

from epics import PV


class Acquisition(QThread):
    def __init__(self):

        QThread.__init__(self) 

        # logging
        logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M:%S')

        self.logger=logging.getLogger('Adaptive Orbit - Acquisition')
        self.logger.info('started at %s' % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        self.logger.info('Version: 1.0.1 ')
        self.logger.info('Host: %s' % socket.gethostname())
        self.channels=[]
        self.nsam=100
        self.isam=0
        self.count=0
        self.validate=[]


    def setup(self,channels,nsam):

        self.nsam=nsam
        self.validate.clear()   # channel ids for validation check
        self.channels.clear()
        chan0=['SIN-CVME-TIFGUN-EVR0:BUNCH-1-OK','SIN-CVME-TIFGUN-EVR0:BUNCH-2-OK']  # fundamental channels at 100 Hz

        for i,cha in enumerate(channels):
            self.channels.append(cha)
            if cha in chan0:
                self.validate.append(i)

        self.isRunning=False
        self.doStop=False
        self.pause=False
        self.addNoise=False

            
    def togglePause(self):
        self.pause=not self.pause
        if self.pause:
            self.status='Paused'
        else:
            self.status='Running'

    def flush(self):
        self.data*=0
        self.isam=0
        self.count=0
        
    def stop(self):
        self.doStop=True
        self.status='Stopped'

    def start(self):
        if self.isRunning:
            return
        self.logger.info('Start Thread for Scan') 
        
        self.data=np.zeros((self.nsam,len(self.channels)+1))
        print(self.data.shape)
        Thread(target=self.core).start()

    def core(self):
        self.isRunning=True
        self.doStop=False
        self.status='Running'
        self.isam=0
        self.count=0
        itry=1000
        nlen=len(self.channels)
        with source(channels=self.channels) as stream:
            while not self.doStop:
                msg=stream.receive()   # read BS 
#                print(msg.data.pulse_id)
                if self.pause:
                    continue
                # check for wrong readout
                skip=False
                for chn in self.channels:
                    if msg.data.data[chn].value is None:
                        skip=True

                if len(self.validate) > 0:
                    for i in self.validate:
                        if msg.data.data[self.channels[i]].value == 0:   
                            skip=True

                if skip:
                    itry-=1
                    if itry<0:
                        self.doStop=True
                        print('too many unsuccessful readings')
                    continue
                else:
                    itry=1000
                self.data[self.isam,nlen]=msg.data.pulse_id
                for i,ele in enumerate(self.channels):
                           self.data[self.isam,i]=msg.data.data[self.channels[i]].value
                self.isam=(self.isam+1) % self.nsam
                self.count+=1
            
        self.logger.info('Stopping Thread for Scan')
        self.isRunning=False
